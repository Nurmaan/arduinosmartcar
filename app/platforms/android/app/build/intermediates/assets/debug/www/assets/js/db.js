function initialiseTable(success) {
    db = window.openDatabase(
        'general.db', '1.0', 'general database', 1000000);
    db.transaction(function (tx) {
        //tx.executeSql("DROP TABLE IF EXISTS quote");
        tx.executeSql("CREATE TABLE IF NOT EXISTS plan  (id VARCHAR(255) PRIMARY KEY, name VARCHAR(255) NOT NULL);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS workout  (id VARCHAR(255) PRIMARY KEY, name VARCHAR(255) NOT NULL, interval INT(11), planID VARCHAR(255) NOT NULL);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS quote  (id VARCHAR(255) PRIMARY KEY, author VARCHAR(255) NOT NULL, quote VARCHAR(255) NOT NULL);");
        tx.executeSql("CREATE TABLE IF NOT EXISTS option  (id VARCHAR(255) PRIMARY KEY, key VARCHAR(255) UNIQUE NOT NULL, value VARCHAR(255));", [], function () {
            getOption("init", value => {
                if (!value || value == "1") {
                    setOption("workoutVibrate", "1", true);
                    setOption("workoutVibrateDuration", "1", true);
                    setOption("workoutNotificationPath", "default", true);
                    setOption("roundVibrate", "1", true);
                    setOption("roundVibrateDuration", "2", true);
                    setOption("roundNotificationPath", "default", true);
                    setOption("workoutLeadTime", "0", true);
                    setOption("init", "0", true);
                    setOption("showQuote", "1", true);
                    setOption("showQuoteInterval", "45", true);
                }
            });

        });
    }, databaseError, function () {
        if (typeof success == "function") success();
        //displayTable('update');
    });
}

function resetDatabase() {
    confirm("Reset All?", "Are Sure You Want to Reset <string>Everything</strong>? You can backup your data and restore it afterwards if you wat to prevent dataa loss", a => {
        db.transaction(function (tx) {
            tx.executeSql("DROP TABLE IF EXISTS plan", [], function () {
                console.log("table dropped");
                tx.executeSql("DROP TABLE IF EXISTS workout", [], function () {
                    console.log("table dropped");
                    tx.executeSql("DROP TABLE IF EXISTS quote", [], function () {
                        console.log("table dropped");
                        tx.executeSql("DROP TABLE IF EXISTS option", [], function () {
                            console.log("table dropped");
                            initialiseTable(a => {
                                toastr.success("All data has been reset", "Reset");
                            });
                        }, function () { console.log("failed to drop table") });
                    }, function () { console.log("failed to drop table") });
                }, function () { console.log("failed to drop table"); });
            }, function () { console.log("failed to drop table") });

        }, databaseError, databaseSuccess)
    });



}

function displayTable(table) {
    //console.log("SELECT * FROM '" + table + "';");
    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM '" + table + "';", [], function (tx, rx) {
            console.log("DISPLAY TABLE: " + table);
            var rows = rx['rows'];
            //console.log(JSON.stringify(rows));
            var count = 0;
            for (var i = 0; i < rows.length; i++) {
                var row = rows[i];
                var disp = "";
                var col = "";
                for (let key in row) {
                    col += key + " ";
                    disp += row[key] + " ";
                }
                if (count == 0) console.log(col);
                console.log(disp);
                count++;
            }
        });
    }, databaseError, databaseSuccess);
}

function databaseError(error) {
    toastr.error("An error occured with your local database");
    //toastr.error(JSON.stringify(error));
    console.log("DATABASE ERROR", error, JSON.stringify(error['message']), error.toString());
}

function databaseSuccess() {
    //toastr.info("Database transaction successful");
}
/*------------------- General ------------------*/
function getTable(table, success) {
    console.log("GETTING TABLE", table);
    db.transaction(function (tx) {
        tx.executeSql("SELECT * FROM '" + table + "';", [], function (tx, rx) {
            //console.log(rx.rows.length, typeof success);
            if (typeof success == "function") success(rx.rows);
        });
    }, databaseError);
}

function singleTransaction(query, params, success, error) {
    console.log("SINGLE TRANSACTION", query, params);
    db.transaction(function (tx) {
        tx.executeSql(query, params, function (tx, rx) {
            if (typeof success == "function") success(rx.rows, tx, rx);
        }, function (tx, err) {
            if (typeof error == "function") error(tx, err);
            databaseError(err);
        });
    }, databaseError);
}

function truncateAddDB() {
    db.transaction(function (tx) {
        tx.executeSql("DELETE FROM 'update'", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
        tx.executeSql("DELETE FROM dishCategory", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
        tx.executeSql("DELETE FROM shop", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
        tx.executeSql("DELETE FROM shopType", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
        tx.executeSql("DELETE FROM shopLocation", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
        tx.executeSql("DELETE FROM shopMenu", [], function () { console.log("table dropped") }, function () { console.log("failed to drop table") });
    });

}

function truncateTable(table, success, error) {
    db.transaction(function (tx) {
        tx.executeSql("DELETE FROM '" + table + "';", [], function (tx, rx) {
            if (typeof success == "function") success(rx);
        }, function (err) {
            if (typeof error == "function") error(rx);
        }
        );
    }, databaseError);
}

/*---------------- Option ----------------*/

function setOption(key, value, insert, callback = null) {
    if (insert) {
        getOption(key, innerValue => {
            if (!innerValue) {
                var id = NewGuid();
                singleTransaction("INSERT INTO option (id, key, value) VALUES (?,?, ?)", [id, key, value.toString()], function (row) {
                    if (typeof callback == "function") callback(row);
                });
            } else {
                setOption(key, value, false, callback);
            }
        });

    } else {
        singleTransaction("UPDATE option SET value=? WHERE key=?", [value, key], function (row) {
            if (typeof callback == "function") callback(row);
        });
    }
}

function getOption(key, callback = null) {
    singleTransaction("SELECT value FROM option WHERE key = ?", [key], function (rows) {
        if (typeof callback == "function") {
            if (rows.length > 0) callback(rows[0]['value']);
            else callback(false);
        }
    });
}