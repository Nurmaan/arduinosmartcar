import os

text = ""
for root, dirs, files in os.walk("./res/font"):
    for filename in files:
        text += "@font-face {\n\tfont-family:" + filename.split(
            ".")[0] + ";\n\tsrc: url('./res/font/" + filename + "');\n}\n"

f = open("./css/fonts.css", "w+")
f.write(text)
f.close()
