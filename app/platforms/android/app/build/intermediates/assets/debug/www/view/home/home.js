var app = {
    macAddress: "98:D3:71:FD:44:A2",
    dataCounter: 0,
    data: [],
    availableDevices: [],
    isDeviceShown: false,
    vibrateReady: true,

    initialize: function () {
        this.bindEvents();
        console.log("Starting SimpleSerial app");
    },

    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    onDeviceReady: function () {
        app.display('Device Ready');
        app.macAddress = localStorage.getItem("macAddress") || "98:D3:71:FD:44:A2";
        setTrim({ target: { value: localStorage.getItem("trim") || 50 } });
        bluetoothSerial.isEnabled(
            listPorts,
            function () {
                app.display("Bluetooth is not enabled.");
                toastr.warning("Connectivity", "Bluetooth is not Enabled");
            }
        );
    },
    openPort: function () {
        app.display("Connected to: " + app.macAddress);
        toastr.success("Connected", "Connection");
        connectButton.classList.add("dark");
        connectButton.classList.remove("light");
        localStorage.setItem("macAddress", app.macAddress);
        bluetoothSerial.subscribe('\n', function (data) {
            app.display(data);
            app.treatInput(data);
        });
    },

    closePort: function () {
        app.display("Disconnected from: " + app.macAddress);
        toastr.success("Connection", "Disconnected");
        connectButton.classList.remove("dark");
        connectButton.classList.add("light");
        bluetoothSerial.unsubscribe(
            function (data) {
                app.display(data);
            },
            app.showError
        );
    },
    showError: function (error) {
        app.display(error);
        toastr.error("Connection Failed", error);
        data = [];
        setTimeout(function () { app.manageConnection(); }, 1000);
    },
    display: function (message) {
        document.getElementById("message").appendChild(createElement("div", {}, [], { innerHTML: message }));
        $('#message').animate({ scrollTop: $('#message')[0].scrollHeight }, 0);
    },
    clear: function () {
        document.getElementById("message").innerHTML = "";
    },
    addData: function (prefix, msg, success, error) {
        if (msg != "") {
            msg = msg < 0 ? 0 : (msg > 99 ? 99 : msg);
            msg = msg < 10 ? "0" + msg : msg + "";
            msg = prefix + msg;
            var element = { msg: msg, success: success, error: error };
            this.data.push(element);
            this.continuousData();
        }

    },
    continuousData: function () {
        setTimeout(function () {
            if (this.dataCounter < this.data.length) {
                if (this.dataCounter > 0 && this.data[this.dataCounter - 1]['msg'] == this.data[this.dataCounter]['msg']) {
                    if (typeof this.data[this.dataCounter]['success'] == "function") this.data[this.dataCounter]['success']();
                    this.dataCounter++;
                    this.continuousData();
                    return;
                }
                bluetoothSerial.write(this.data[this.dataCounter]['msg'], function () {
                    if (typeof this.data[this.dataCounter]['success'] == "function") this.data[this.dataCounter]['success']();
                    this.dataCounter++;
                    this.continuousData();
                }.bind(this), function () {
                    app.display('failed to transmit');
                    if (typeof this.data[this.dataCounter]['error'] == "function") this.data[this.dataCounter]['error']();
                    this.dataCounter++;
                    this.continuousData();
                }.bind(this));
            }
        }.bind(this), 70);

    },
    treatInput: function (data) {
        if (data == 1 && this.vibrateReady) {
            this.vibrateReady = false;
            window.navigator.vibrate(200);
            setTimeout(function () { app.vibrateReady = true }, 500);
        }
    },
    manageConnection: function () {

        var connect = function () {
            app.display("Attempting to connect. " +
                "Make sure the serial port is open on the target device.");
            toastr.warning("Attempting to connect", "Connection");
            bluetoothSerial.connect(
                app.macAddress,
                app.openPort,
                app.showError
            );
        };

        var disconnect = function () {
            app.display("attempting to disconnect");
            toastr.warning("Attempting to disconnect", "Connection");
            bluetoothSerial.disconnect(
                app.closePort,
                app.showError
            );
        };

        bluetoothSerial.isConnected(disconnect, connect);
    }
};

function listPorts() {
    app.display("Listing Ports...");
    bluetoothSerial.list(
        function (results) {
            app.display(JSON.stringify(results));
            app.availableDevices = results;
            displayDevice();
        },
        function (error) {
            app.display(JSON.stringify(error));
        }
    );
}


function displayDevice() {
    var target = document.getElementById("device");
    for (var result of app.availableDevices) {
        target.appendChild(createElement("div", { class: "deviceItemContainer", onclick: "connectTo('" + result.address + "')" }, [
            createElement("div", { class: "deviceItemName" }, [], { innerHTML: result.name }),
            createElement("div", { class: "deviceItemAddress" }, [], { innerHTML: result.address })
        ]));
    }
    if (target.innerHTML == "") target.innerHTML = "No Data";
}

function thumbStickListener() {
    setHandlers(document.getElementById("thumb1"));
    setHandlers(document.getElementById("thumb2"));
}

function setHandlers(elem) {
    elem.ontouchstart = touchStart;
    elem.ontouchmove = touchMove;
    elem.ontouchend = touchEnd;
}

function touchStart(e) {
    for (var targetTouch of e.targetTouches) {
        var target = targetTouch.target;
        target.setAttribute("clientX", targetTouch.clientX);
        target.setAttribute("clientY", targetTouch.clientY);
    }
}

function touchMove(e) {
    for (var targetTouch of e.targetTouches) {
        var target = targetTouch.target,
            oldX = target.getAttribute("clientX"),
            oldY = target.getAttribute("clientY"),
            diffX = targetTouch.clientX - oldX,
            diffY = targetTouch.clientY - oldY,
            refDim = document.body.offsetHeight / 2,
            percentX = (diffX / refDim) * 100,
            percentY = (diffY / refDim) * 100;

        percentX = 50 + percentX;
        percentY = 50 + percentY;
        percentX = percentX < 0 ? 0 : (percentX > 100 ? 100 : percentX);
        percentY = percentY < 0 ? 0 : (percentY > 100 ? 100 : percentY);

        target.style.top = "calc(" + percentY + "% - 12.5vh)";
        target.style.left = "calc(" + percentX + "% - 12.5vh)";

        //send data
        if (target.id == "thumb1") {
            app.addData("t", Math.round(percentY - 1));
        } else {
            app.addData("s", Math.round(percentX - 1));
            if (percentY == 0) {
                var target = document.getElementById("headlight");
                target.classList.toggle("active");
                target.classList.toggle("fa-lightbulb");
                target.classList.toggle("fa-lightbulb-on");
                app.addData("l", target.classList.contains("active") ? 1 : 2);
            } else if (percentY == 100) {
                var target = document.getElementById("taillight");
                target.classList.toggle("active");
                target.classList.toggle("fa-lightbulb");
                target.classList.toggle("fa-lightbulb-on");
                app.addData("l", target.classList.contains("active") ? 3 : 4);
            }
        }
    }
}

function touchEnd(e) {
    var target = e.target;
    target.style.top = "calc(50% - 12.5vh)";
    target.style.left = "calc(50% - 12.5vh)";

    //send data
    if (target.id == "thumb1") {
        app.addData("t", 50);
    } else {
        app.addData("s", 50);
    }
}

function showMessage() {
    document.getElementById("messageContainer").classList.add("active");
}
function showDevice() {
    document.getElementById("deviceContainer").classList.add("active");
}
function showTrim() {
    document.getElementById("trimContainer").classList.add("active");
}

function hideMessage() {
    document.getElementById("messageContainer").classList.remove("active");
}
function hideDevice() {
    document.getElementById("deviceContainer").classList.remove("active");
}
function hideTrim() {
    document.getElementById("trimContainer").classList.remove("active");
}

function setTrim(e) {
    var value = parseInt(e.target.value);
    localStorage.setItem("trim", value);
    document.getElementById("trimSlider").value = value;
    document.getElementById("trimValue").innerHTML = value == 50 ? "0&#186;" : (value < 50 ? 50 - value : value - 50) + "&#186; " + (value > 50 ? "Right" : "Left");
    app.addData("y", value - 1);
}

function resetTrim(e) {
    setTrim({ target: { value: "50" } });
    document.getElementById("trimSlider").value = 50
}
function incrementTrim(add) {
    var current = parseInt(document.getElementById("trimSlider").value);
    current = add ? current + 1 : current - 1;
    current = current < 0 ? 0 : (current > 100 ? 100 : current);
    setTrim({ target: { value: current.toString() } });

}

function toggleHeadlight(e) {
    var target = e.target;
    target.classList.toggle("active");
    target.classList.toggle("fa-lightbulb");
    target.classList.toggle("fa-lightbulb-on");
    app.addData("l", target.classList.contains("active") ? 1 : 2);
}

function toggleTaillight(e) {
    var target = e.target;
    target.classList.toggle("active");
    target.classList.toggle("fa-lightbulb");
    target.classList.toggle("fa-lightbulb-on");
    app.addData("l", target.classList.contains("active") ? 3 : 4);
}

function connectTo(address) {
    app.macAddress = address;
    app.manageConnection();
    hideDevice();
}