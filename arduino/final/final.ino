#include <Servo.h>
 
unsigned int val;
#define in1 4
#define in2 6
#define echoPinRear 4  
#define echoPinFront 13
#define led1 6
#define led4 8
#define led3 7
#define led2 9
#define steerPin 3
#define enA 5
#define pir 12
#define buzzer 5
#define ldr A0
#define trigPin A1
#define trigPin2 A2
#define brakePin A3         
int lastSpeed;
boolean dir = 1;
boolean autoLight = false;
boolean brake = false  ;
boolean autoBrake = false;
 
int alarm = 0;
int flash = 0;
int light = 0;
int oldLight = 0;
int distFront = 0;
int distBack = 0;
/*
*       LED
* BLUE headlights
* ORANGE tailights n    
 * YELLOW flasher right
* GREEN flasher left
*
*/
Servo steerServo;
Servo brakeServo;
 
void setup() {
  Serial.begin(9600);
  Serial.setTimeout(10 );
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(led4, OUTPUT);
  pinMode(pir, INPUT);
  pinMode(trigPin, OUTPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPinRear, INPUT);
  pinMode(echoPinFront, INPUT);
  Serial.begin(9600);
  Serial.setTimeout(20);
  steerServo.attach(steerPin);
  steerServo.write(90);
  brakeServo.attach(brakePin);
  brakeServo.write(90);
 
 
  directionChange(0);
}
 
void loop() {
  if(autoBrake){checkDistance();}
  checkBrake();
  if(alarm == 1){
    int pirValue = digitalRead(pir);
    if (pirValue == 1){
      tone(buzzer, 2000);
      flash = 3;
      Serial.println(7);
      alarm = false;
    }
  }else if(alarm == 2){
    noTone(buzzer);
    flash = 0;
    alarm = 0;
  }
  while(Serial.available() > 0){
    val = Serial.parseInt();
  }
  if(val > 0){
    //Serial.println(val);
    alarm = 0;
    switch(val){
     
        /*
         * right: on => 11 off => 10
         * left: on => 13 off => 12
         * hazard: on => 15 off => 14
         * normal: on => 17 off => 16
         * fullLight: on => 19 off => 18
         * flash: on => 20 off => 21
         * alarm: on => 23 off => 22
         * autoLight: on => 25 off => 24
         * alarmStop: 26
         *
         * flash: 0 => off 1 => right 2 => left 3 => hazard
         * light: 0 => off 1 => normal 2 => flash 3 => full
         */
        
      case 10:
        flash = 0;
        break;
       
      case 11:
        flash = 1;
        break;
       
      case 12:
        flash = 2;
        break;
       
      case 13:
        flash = 3;
        break;
       
      case 14:
        light = 0;
        autoLight = false;
        break;
       
      case 15:
        light = 1;
        autoLight = false;
        break;
 
      case 16:
        light = 3;
        autoLight = false;
        break;
       
      case 18:
        light = autoLight ? 0 : oldLight;
        break;
       
      case 17:     
        oldLight = light;
        light = 2;
        break;
 
      case 21:
        autoLight = false;
        break;
       
      case 22:
        autoLight =  true;
        break;
 
      case 19:
        alarm  = 0;
        break;
 
      case 20:
        alarm = 1;
        flash = 0;
        light = 0;
        autoLight = false;
        break;
 
      case 23:
        alarm = 2;
        break;
 
      case 24:
        brake = false;
        break;
 
      case 25:
        brake = true;
        break;
 
      case 26:
        autoBrake = false;
        brake = false;
        break;
 
      case 27:
        autoBrake = true;
        break;
       
      default:
        analogHandler(val);
        break;
    }
 
    val=0;
  }
 
  if(autoLight){
    int ldrValue = analogRead(ldr);
    if(ldrValue < 30){
      light = light == 2 ? 2 : 1;
    }else if(ldrValue > 30){
      light = light == 2 ? 2 : 0;
    }
  }
 
  lighting();
  flasher();
}
void flasher(){
  switch(flash){
    case 0:
      digitalWrite(led4, LOW);
      digitalWrite(led3, LOW);
      break;
 
    case 1:
      digitalWrite(led4, HIGH);
      digitalWrite(led3, LOW);
      delay(25);
      digitalWrite(led4, LOW);
      delay(100);
      break;
 
    case 2:
      digitalWrite(led3, HIGH);
      digitalWrite(led4, LOW);
      delay(25);
      digitalWrite(led3, LOW);
      delay(100);
      break;
 
    case 3:
      digitalWrite(led3, HIGH);
      digitalWrite(led4, HIGH);
      delay(25);
      digitalWrite(led3, LOW);
      digitalWrite(led4, LOW);
      delay(100);
      break;
  }
}
void lighting(){
  switch(light){
    case 0:
      analogWrite(led1, 0);
      analogWrite(led2, 0);
      break;
 
    case 1:
      analogWrite(led1, 50);
      analogWrite(led2, 255);
      break;
 
    case 2:
      analogWrite(led1, 255);
      break;
 
    case 3:
      analogWrite(led1, 255);
      analogWrite(led2, 50);
      break; 
  }
}
void analogHandler(int val){
  //1 - 4 - 7
  //8 - 10 - 12
 
  if(val >= 5){
    steerProp(val);
  }else{
    speedHandler(val);
  }
}
void speedHandler(int spd){
 
   switch(spd){
    case 0:
      directionChange(0);
      speedTo(2);
      break;
    case 1:
      directionChange(0);
      speedTo(2);
      break;
    case 2:
      directionChange(0);
      speedTo(0);
      break;
    case 3:
      directionChange(1);
      speedTo(1);
      break;
    case 4:
      directionChange(1);
      speedTo(2);
      break;
    default:
      directionChange(0);
      speedTo(0);
      break;
   
    }
}
void steerProp(int angle){
  switch(angle){
    case 5:
      angle = 65;
      break;
    case 6:
      angle = 78;
      break;
    case 7:
      angle = 90;
      break;
    case 8:
     angle = 100;
      break;
    case 9:
      angle = 110;
      break;
    default:
      angle = 90;
      break;
    }
  steerServo.write(angle);
}
void reverseDirection(){ 
  directionChange(dir == 0 ? 1 : 0);
}
void directionChange(int type){
  dir = type;
  if(type == 1){     
      
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
       
  }else if(type == 0){   
        
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
     
  }
}
void speedTo(int spd){
  switch(spd){
 
    case 1:
      spd = 128;
      break;
    case 2:
      spd = 255;
      break;
    case 0:
      spd = 0;
      break;
    }
  analogWrite(enA, spd); 
  lastSpeed = spd;
 
}
int getDistance(boolean front){
  long duration;
  int distance;
  digitalWrite(front ? trigPin : trigPin2, HIGH);
  delayMicroseconds(10);
  digitalWrite(front ? trigPin : trigPin2, LOW);
  duration = pulseIn(front ? echoPinFront : echoPinRear, HIGH, 55000);
  distance= duration*0.034/2;
  return distance;
}
void checkDistance(){
  int front = treatDistance(getDistance(1));
  int back = treatDistance(getDistance(0));
  if(front != distFront){
    Serial.println(front);
    distFront = front; 
  }
 
  if(back != distBack){
    Serial.println(7 - back);
    distBack = back; 
  }
 
  if((dir ? front : back) < 10){
    brake = true;  
  }else{
    brake = false; 
  }
}
int treatDistance(int input){
  int ret;
  if(input > 25){
    ret = 1;
  }  else if(input > 10){
    ret = 2; 
  } else if(input < 10){
    ret = 3;
  }
 
  return ret;
}
int getSpeed(){
  int distance1 = getDistance(dir);
  delayMicroseconds(100);
  int distance2 = getDistance(dir);
  int spd = (distance1 - distance2)*2;
  return spd;
}
void checkBrake(){
  if(brake){
    brakeServo.write(55); 
  }else{
    brakeServo.write(90);
  }
}
