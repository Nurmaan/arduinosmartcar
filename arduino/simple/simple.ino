#include <Servo.h>
#include <EEPROM.h>

String val;
#define headlight 7
#define steerPin 3
#define in1 4
#define enA 5
#define in2 6
#define taillight 2
#define vibration 8

int steerOrigin = 95;
int steerOriginNew = 95;
int cycles = 0;

Servo steerServo;
 
void setup() {
  pinMode(headlight, OUTPUT);
  pinMode(enA, OUTPUT);
  pinMode(in1, OUTPUT);
  pinMode(in2, OUTPUT);
  pinMode(taillight, OUTPUT);
  pinMode(vibration, INPUT);
  Serial.begin(9600);
  steerServo.attach(steerPin);

  //read eeprom
  int eVal = EEPROM.read(0);
  if(eVal != 0 && eVal != 255){
    Serial.println(EEPROM.read(0));
    steerOriginNew = EEPROM.read(0);
  }
  
  steerServo.write(steerOriginNew); 
}
 
void loop() {
  checkVibration();
  while(Serial.available()){
    char str[3];
    Serial.readBytes(str, 3);
    String s = str;
    treatData(s);
  }
  cycles++;
}

void checkVibration(){
  //if(digitalRead(vibration) == HIGH && cycles%1000 == 0) Serial.println(1);  
}

void treatData(String s){
  switch(s.charAt(0)){
    case 't':
      s.remove(0, 1);
      manageThrottle(s.toInt());
      break;
     case 's':
      s.remove(0, 1);
      manageSteering(s.toInt());
      break;
     case 'y':
      s.remove(0, 1);
      manageTrim(s.toInt());
      break;
     case 'l':
      s.remove(0, 1);
      manageLights(s.toInt());
      break;
  }
}

void manageLights(int val){
  switch(val){
    case 1:
      digitalWrite(taillight, HIGH);
      break;
    case 2:
      digitalWrite(taillight, LOW);
      break;
    case 3:
      digitalWrite(headlight, HIGH);
      break;
    case 4:
      digitalWrite(headlight, LOW);
      break;
  }
}

void manageTrim(int val){
  if(val > 50){
    steerOriginNew = steerOrigin + (val - 50);
  }else{
    steerOriginNew = steerOrigin - (50 - val);
  }
  if(val == 0) steerOriginNew = steerOrigin;
  steerServo.write(steerOriginNew);
  EEPROM.write(0, steerOriginNew);
}

void manageThrottle(int val){
  changeDirection(val < 50);
  val = val > 50 ? val - 50 : 50 - val;
  //Serial.println(map(val, 0, 50, 0, 255));
  analogWrite(enA, map(val, 0, 50, 0, 255));
}

void manageSteering(int val){
  if(val == 150) steerServo.write(steerOriginNew);
  else steerServo.write(map(val, 0,99,55,135));
}

void changeDirection(bool forward){
  //Serial.println(forward);
  if(!forward){
    digitalWrite(in1, HIGH);
    digitalWrite(in2, LOW);
  }else{
    digitalWrite(in1, LOW);
    digitalWrite(in2, HIGH);
  }
}
